﻿capital = 52

#OOB = "ALB_1936"


set_research_slots = 2

set_stability = 0.80

set_war_support = 0.7

set_politics = {

	parties = {
		democratic = { 
			popularity = 32
		}

		fascism = {
			popularity = 5
		}
		
		communism = {
			popularity = 6
		}
		
		neutrality = { 
			popularity = 57
		}
	}
	
	ruling_party = neutrality
	election_frequency = 48
	elections_allowed = no
}


set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	early_fighter = 1
	early_bomber = 1
	early_submarine = 1
	basic_destroyer = 1
	early_destroyer = 1
	early_light_cruiser = 1
	basic_light_cruiser = 1
	early_heavy_cruiser = 1
	transport = 1
	gwtank = 1
	basic_heavy_tank = 1
	support_weapons = 1
	basic_battlecruiser = 1
	basic_battleship = 1
	gw_artillery = 1
}

set_convoys = 0

create_country_leader = {
	name = "Ruprecht I"
	picture = "gfx/leaders/BAV/Ruprecht.dds"
	ideology = despotism

