﻿capital = 468

OOB = "GQU_1936"

set_research_slots = 2

set_stability = 0.5

set_war_support = 0.5

set_technology = {
	infantry_weapons = 2
	tech_support = 1
	tech_recon = 1
}
add_ideas = {
	english_sepratists
		}
		
set_politics = {

	parties = {
		democratic = { 
			popularity = 23
		}

		fascism = {
			popularity = 75
		}
		
		communism = {
			popularity = 2
		}
		
		neutrality = { 
			popularity = 0
		}
	}
	
	ruling_party = fascism
	last_election = "1932.3.5"
	election_frequency = 48
	elections_allowed = no
}

set_convoys = 175

create_country_leader = {
	name = "Maurice Duplessis"
	picture = "gfx/leaders/GQU/Maurice_Duplessis.dds"
	ideology = Conservatism

}
create_country_leader = {
	name = "Lionel Groulx"
	picture = "gfx/leaders/GQU/Lionel_Groulx.dds"
	ideology = rexism

}

}


create_corps_commander = {
	name = "Maxime Weygand"
	portrait_path = "gfx/leaders/GQU/Maxime Weygand.dds"
	traits = {  }
	skill = 3
    attack_skill = 4
    defense_skill = 1
    planning_skill = 2
    logistics_skill = 3
}



